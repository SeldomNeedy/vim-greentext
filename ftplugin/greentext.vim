" Convert from a desuarchive link to a native cross-thread or cross-board link:
command! -buffer GreentextFromDesu
	\ let s:oldgdefault=&gdefault
	\ | set nogdefault
	\ | sm'http\(s\?\)://desuarchive.org/\([a-z0-9]\{1,4}\)/thread/\(\d\+\)/#\(\d\+\)'http\1://boards.4chan.org/\2/thread/\3#p\4'
	\ | execute s:oldgdefault? 'set gdefault' : 'set nogdefault'
