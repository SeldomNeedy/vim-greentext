" Vim syntax file
" Language: Greentext
" Maintainer: Seldom Needy
" Latest Revision: 25 December 2019

" Lines starting with '>' are green:
syn region greenLine start='^>[^>]' end='$' contains=ALL keepend
hi def link greenLine Grn
hi Grn ctermfg=Green guifg=Green

" A string of numbers preceded by '>>' is assumed to be a reference to a post
" in the current thread:
syn match postRef '>>\d\+\( \)\@=\|>>\d\+$'
hi def link postRef Red
hi Red ctermfg=Red guifg=Red

" Any characters between '[spoiler]' and '[/spoiler]' get a background
" highlight:
syn region spoil start='\(\[spoiler\]\)\@<=' end='\(\[/spoiler]\)\@='
hi def link spoil Wht
hi Wht ctermbg=White
