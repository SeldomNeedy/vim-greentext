augroup greentext_ft
	autocmd!
	autocmd BufNewFile,BufRead *.green.txt  set ft=greentext
	" We have to use set ft because filetype 'text' will be recognized first,
	" and immediately set, leaving us to override it.
augroup end
